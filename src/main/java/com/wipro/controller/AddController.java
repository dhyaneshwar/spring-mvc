package com.wipro.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.servlet.ModelAndView;

import com.wipro.service.interfaces.MVCService;
import com.wipro.service.interfaces.Shape;

@Controller
public class AddController {
	
	@Autowired
	private MVCService mvcService;
	
	@Autowired
	private Shape triangle;
	
	private static final Logger logger = Logger.getLogger(AddController.class);
	
	@RequestMapping("/add")
	public ModelAndView add(@RequestParam("t1") int i,@RequestParam("t2") int j, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model = new ModelAndView();
		int result;
		try {
			//request.getSession().invalidate();
			
			logger.info("This is info message");
			System.out.println("Inside Add controller : ");
			
			/*
			 * int i = Integer.parseInt(request.getParameter("t1")); int j =
			 * Integer.parseInt(request.getParameter("t2"));
			 */
			result = mvcService.add(i, j);
			System.out.println("Result : " + result);
			triangle.draw();
			model.setViewName("display");
			model.addObject("result", result);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return model;
	
	}

}
