package com.wipro.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wipro.model.UserRegistrationDTO;

@Controller
public class RegistrationController {
	
	@RequestMapping("/register")
	public String showRegistrationPage(@ModelAttribute("userReg") UserRegistrationDTO dto) {
		
		return "user-registration-page";
	}
	
	@RequestMapping("/registrationSuccess")
	public String processUserRegistration(@ModelAttribute("userReg") UserRegistrationDTO dto) {
		
		return "registrationSuccess";
	}

}
