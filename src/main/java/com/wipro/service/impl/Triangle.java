package com.wipro.service.impl;

import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertySources;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.wipro.service.interfaces.Shape;



@Service("triangle")
@PropertySource("classpath:pointsConfig.properties")
public class Triangle implements Shape , ApplicationEventPublisherAware{

	/*
	 * public Triangle(){
	 * 
	 * }
	 */
	/*
	 * public Triangle(String type) { this.type = type; } public Triangle(String
	 * type,int height) { this.type = type; this.height = height; }
	 * 
	 * private String type; private int height;
	 * 
	 * public String getType() { return type; }
	 * 
	 * public void setType(String type) { this.type = type; }
	 */

	
	  @Autowired
	  private ObjectProvider<Point> pointA;
	  
	  @Autowired
	  private Shape circle;
	 
	
	// @Autowired private Point pointA;
	 /* 
	 * @Autowired private Point pointC;
	 */
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private Environment env;
	@Autowired
	private Properties myProperties;
	private ApplicationEventPublisher publisher;
	@Value("${point1.x}")
	private String propstring;

	// private List<Point> points;

	/*
	 * public Point getPointA() { return pointA; }
	 * 
	 * public void setPointA(Point pointA) { this.pointA = pointA; }
	 * 
	 * public Point getPointB() { return pointB; }
	 * 
	 * public void setPointB(Point pointB) { this.pointB = pointB; }
	 * 
	 * public Point getPointC() { return pointC; }
	 * 
	 * public void setPointC(Point pointC) { this.pointC = pointC; }
	 * 
	 * 
	 * 
	 * public MessageSource getMessageSource() { return messageSource; }
	 * 
	 * public void setMessageSource(MessageSource messageSource) {
	 * this.messageSource = messageSource; }
	 */
	@Override
	public String draw2(String name) {
		System.out.println("Inside Triangle Class : draw2() --> Triangle Drawn");
		System.out.println("Point A --> X : " + this.pointA.getIfAvailable().getX() + " , Y : " + this.pointA.getIfAvailable().getY());
		return null;
		
	}
	@Override
	public void draw() {
		System.out.println("Inside Triangle Class : draw() --> Triangle Drawn");
		/*
		 * System.out.println("Type : "+ this.type); System.out.println("Height : "+
		 * this.height);
		 */
		 

		
		System.out.println("Point A --> X : " + this.pointA.getIfAvailable().getX() + " , Y : " + this.pointA.getIfAvailable().getY());
		/*
		 * System.out.println("Point B --> X : " + this.pointB.getX() + " , Y : " +
		 * this.pointB.getY()); System.out.println("Point C --> X : " +
		 * this.pointC.getX() + " , Y : " + this.pointC.getY());
		 */
		if(this.pointA.getIfAvailable().getX()==0 && this.pointA.getIfAvailable().getY() == 0) {
			this.pointA.getIfAvailable().setX(15);
			this.pointA.getIfAvailable().setY(20);
		}
		
		circle.draw();
		System.out.println("After calling circle");
		System.out.println("Point A --> X : " + this.pointA.getIfAvailable().getX() + " , Y : " + this.pointA.getIfAvailable().getY());
		 
		
		String message = this.messageSource.getMessage("point1.x", null, "not found", null);
		System.out.println("Inside Triangle Draw : Message source value : " + message);
		System.out.println(
				"Inside Triangle Draw : Message source value from Property placeholder configurer : " + propstring);
		System.out.println("Inside Triangle Draw : Message source value from Java Util Property : "
				+ myProperties.getProperty("point1.x"));
		System.out.println(
				"Inside Triangle Draw : Message source value from Environment : " + env.getProperty("point1.x"));
		 
		
		/*
		 * DrawEvent drawEvent = new DrawEvent(this); publisher.publishEvent(drawEvent);
		 */
		

		/*
		 * for(Point point : this.points) { System.out.println("Point --> X : " +
		 * point.getX() + " , Y : " + point.getY()); }
		 */
	}

	
	
	@Override
	public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {
		this.publisher = publisher;

	}



	
	 
	 

	/*
	 * public List<Point> getPoints() { return points; }
	 */

	/*
	 * public void setPoints(List<Point> points) { this.points = points; }
	 */

}
