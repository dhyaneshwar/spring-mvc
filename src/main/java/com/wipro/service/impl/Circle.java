package com.wipro.service.impl;


import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.wipro.aspect.LoggerAnnot;
import com.wipro.service.interfaces.Shape;



@Component("circle")
@PropertySource("classpath:pointsConfig.properties")
public class Circle implements Shape{
	
	@Autowired
	private ObjectProvider<Point> pointA;

	
	@Override
	public void draw() {
		System.out.println("Inside Circle Class : draw() --> Circle Drawn");
		System.out.println("Point A --> X : " + this.pointA.getIfAvailable().getX() + " , Y : " + this.pointA.getIfAvailable().getY());
		if(this.pointA.getIfAvailable().getX()==0 && this.pointA.getIfAvailable().getY() == 0) {
			this.pointA.getIfAvailable().setX(50);
			this.pointA.getIfAvailable().setY(60);
		}
	}

	@Override
	/* @LoggerAnnot */
	public String draw2(String name){
		System.out.println("Inside Circle Class : draw2() --> Circle Drawn2 : Name : " + name + " is a gud boy");
		try {
			throw new RuntimeException();
		}
		catch(Exception e) {
			System.out.println("caught in catch block : " + e);
		}
			
		return name + " is a gud boy";
	}
	
	
	 
}
