package com.wipro.service.impl;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class MyEventListener implements ApplicationListener<ApplicationEvent>{

	@Override
	public void onApplicationEvent(ApplicationEvent event) {
		if(event instanceof DrawEvent) {
			System.out.println("Inside MyEventListener : " + event.toString());	
		}
		
		
	}
	@EventListener
	public void onAnnotEventListner(ApplicationEvent event) {
		if(event instanceof DrawEvent) {
			System.out.println("Inside MyAnnotEventListener : " + event.toString());	
		}
		
		
	}

}
