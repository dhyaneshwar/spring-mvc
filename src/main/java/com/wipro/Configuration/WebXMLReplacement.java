
package com.wipro.Configuration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.context.annotation.AnnotationConfigRegistry;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class WebXMLReplacement// implements WebApplicationInitializer
{

//	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		System.out.println("Inside Web xml Replacement class");
		XmlWebApplicationContext webApplnContext = new XmlWebApplicationContext();
		webApplnContext.setConfigLocation("/WEB-INF/context-servlet.xml");
		/*
		 * AnnotationConfigWebApplicationContext webApplnContext = new
		 * AnnotationConfigWebApplicationContext();
		 * webApplnContext.register(MVCConfig.class);
		 */
		DispatcherServlet dispatcherServlet = new DispatcherServlet(webApplnContext);

		Dynamic customDispatcherServlet = servletContext.addServlet("context", dispatcherServlet);
		customDispatcherServlet.setLoadOnStartup(1);
		customDispatcherServlet.addMapping("/");
	}
}
