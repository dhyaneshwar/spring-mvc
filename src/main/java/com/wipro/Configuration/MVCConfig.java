
package com.wipro.Configuration;

import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@EnableWebMvc
@Configuration
@EnableAspectJAutoProxy
@ComponentScan(basePackages = "com.wipro")
public class MVCConfig {

	@Bean
	public InternalResourceViewResolver viewResolver() {
		InternalResourceViewResolver vr = new InternalResourceViewResolver();
		vr.setPrefix("/WEB-INF/views/");
		vr.setSuffix(".jsp");
		return vr;
	}
	
	@Bean
	public ResourceBundleMessageSource messageSource() {
		
		ResourceBundleMessageSource messageSource =  new ResourceBundleMessageSource();
		try {
			messageSource.addBasenames("pointsConfig");
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return messageSource;
		
	}
	
	@Bean
	public PropertiesFactoryBean myProperties() {
		
		PropertiesFactoryBean myProperties =  new PropertiesFactoryBean();
		try {
			myProperties.setLocation(new ClassPathResource("pointsConfig.properties"));
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return myProperties;
		
	}

}
