package com.wipro.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LoggingAspect extends PointCutExpressions {

	/*
	 * @Before("execution(public void com.dhyanesh.wipro.Triangle.draw())") public
	 * void loggingAdvice() {
	 * System.out.println("Advise is run :: loggingAdvise is called"); }
	 */

	// @Before("execution(* draw*())")//means no arguments
	/*
	 * @Before("allDrawMethodsWithoutArgs()") public void loggingAdvice() {
	 * System.out.println("Advise is run :: allDrawMethodsWithoutArgs is called"); }
	 * 
	 * // @Before("execution(* draw*(..))")// means o or more arguments
	 * 
	 * @Before("allDrawMethodsIrrespectiveArgs()") public void loggingAdvice2() {
	 * System.out.
	 * println("Advise is run :: allDrawMethodsIrrespectiveArgs is called"); }
	 * 
	 * // @Before("execution(* draw*(*))")// means 1 or more arguments public void
	 * 
	 * @Before("allDrawMethodsWith1orMoreArgs()") public void loggingAdvice3() {
	 * System.out.println("Advise is run :: allDrawMethodsWith1orMoreArgs is called"
	 * ); }
	 */

	/*
	 * @Before("allMethodsWithinCircle()") public void loggingAdvice4() {
	 * System.out.println("Advise is run :: allMethodsWithinCircle is called"); }
	 */

	/*
	 * @Before("allDrawMethodsWithoutArgs() || allMethodsWithinCircle()") public
	 * void loggingAdvice5() {
	 * System.out.println("Advise is run :: allMethodsWithinWipro is called");
	 * 
	 * }
	 */

	/*
	 * @Before("allMethodsWithinCircle()") public void loggingAdvice6(JoinPoint
	 * jointPoint) { System.out.println(jointPoint.toString()); }
	 */

	/*
	 * @Before("allMethodsWithinCircle() && allMethodsWithSingleStringArg()") public
	 * void loggingAdvice7(JoinPoint jointPoint) {
	 * System.out.println("Before Advise");
	 * System.out.println(jointPoint.getArgs()[0]); }
	 * 
	 * @After("allMethodsWithinCircle() && allMethodsWithSingleStringArg()") public
	 * void loggingAdvice8(JoinPoint jointPoint) {
	 * System.out.println("After Advise");
	 * System.out.println(jointPoint.getArgs()[0]); }
	 */

	/*
	 * @AfterReturning(pointcut =
	 * "allMethodsWithinCircle() && allMethodsWithSingleStringArg()",returning
	 * ="returnValue") public void loggingAdvice8(JoinPoint jointPoint,String
	 * returnValue) { System.out.println("After Returning Advise : Class Name : " +
	 * jointPoint.getTarget().getClass() + " Method Name : " +
	 * jointPoint.getSignature()); System.out.println("Method Arg : "+
	 * jointPoint.getArgs()[0]); System.out.println("Method Return Value : " +
	 * returnValue); }
	 */

	@AfterThrowing(pointcut = "allMethodsWithinCircle() && allMethodsWithSingleStringArg()", throwing = "ex")
	public void loggingAdvice9(JoinPoint jointPoint, Exception ex) {
		System.out.println("After Throwing Advise : Class Name : " + jointPoint.getTarget().getClass()
				+ " Method Name : " + jointPoint.getSignature());
		System.out.println("Method Arg : " + jointPoint.getArgs()[0]);
		System.out.println("Method Return Value : " + ex);
	}

	//@Around("allMethodsWithinCircle() && allMethodsWithSingleStringArg()")
	//@Around("allMethodsWithCustomAnnot()")
	@Around("allMethodsWithinCircle()")
	public Object loggingAdvice10(ProceedingJoinPoint proccedingJointPoint) {
		System.out.println(
				"Around logging Advise Before Execution : Class Name : " + proccedingJointPoint.getTarget().getClass().getName()
						+ " Method Name : " + proccedingJointPoint.getSignature().getName());
		
		if(proccedingJointPoint.getArgs()!=null && proccedingJointPoint.getArgs().length > 0) {
			System.out.println("Method Arg : " + proccedingJointPoint.getArgs()[0]);
		}
		
		Object returnValue = null;
		try {
			returnValue = proccedingJointPoint.proceed();
		} catch (Throwable e) {
			System.out.println("Exception caught in Around Advise Catch Block");
			e.printStackTrace();
		}
		System.out.println("Around logging Advise After Execution : Return Value : " + returnValue + " and especially a meme lover");
		return returnValue + " and especially a meme lover";
	}
	

}
