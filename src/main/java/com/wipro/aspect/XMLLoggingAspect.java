package com.wipro.aspect;

import org.aspectj.lang.ProceedingJoinPoint;


public class XMLLoggingAspect {
	
	public Object loggingAdvice10(ProceedingJoinPoint proccedingJointPoint) {
		System.out.println(
				"Around XML logging Advise Before Execution : Class Name : " + proccedingJointPoint.getTarget().getClass()
						+ " Method Name : " + proccedingJointPoint.getSignature());
		
		if(proccedingJointPoint.getArgs()!=null && proccedingJointPoint.getArgs().length > 0) {
			System.out.println("XML Method Arg : " + proccedingJointPoint.getArgs()[0]);
		}
		
		Object returnValue = null;
		try {
			returnValue = proccedingJointPoint.proceed();
		} catch (Throwable e) {
			System.out.println("Exception caught in XML Around Advise Catch Block");
			e.printStackTrace();
		}
		System.out.println("Around XML logging Advise After Execution : Return Value : " + returnValue + " and especially a meme lover");
		return returnValue + " and especially a meme lover";
	}

}
