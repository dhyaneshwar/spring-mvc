<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1 align="center">Please Register here</h1>
	<div align="center">
		<form:form action="/DemoMVC/registrationSuccess" method="GET" modelAttribute="userReg">

			<label>User : </label>
			<form:input path="name" />
			<br />
			<br />


			<label>User Name : </label>
			<form:input path="userName" />
			<br />
			<br />

			<label>Password : </label>
			<form:password path="password" />
			<br />
			<br />

			<label>Country : </label>
			<form:select path="countryName">
				<form:option value="IND" label="India" />
				<form:option value="SG" label="Singapore" />
				<form:option value="HK" label="Hong Kong" />
			</form:select>
			<br />
			<br />

			<label>Hobbies : </label>
			<form:checkbox path="hobbies" value="cricket" />
			Cricket
			<form:checkbox path="hobbies" value="football" />
			Football
			<form:checkbox path="hobbies" value="hockey" />
			Hockey
			<form:checkbox path="hobbies" value="books" />
			Books <br />
			<br />

			<label>Gender : </label>
			<form:radiobutton path="gender" value="MALE" />
			Male
			<form:radiobutton path="gender" value="FEMALE" />
			Female <br />
			<br />

			<input type="submit" value="Register">

		</form:form>
	</div>





</body>
</html>